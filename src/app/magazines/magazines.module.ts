import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MagazinesRoutingModule } from './magazines-routing.module';
import { MagazinesComponent } from './magazines/magazines.component';
import { MagazineComponent } from './magazine/magazine.component';
import { MagazinesService } from '../services/magazines.service';
import { MagazineResolver } from './magazine/magazine.resolver';

@NgModule({
  imports: [
    CommonModule,
    MagazinesRoutingModule
  ],
  declarations: [MagazinesComponent, MagazineComponent],
  providers: [MagazinesService, MagazineResolver]
})
export class MagazinesModule { }
