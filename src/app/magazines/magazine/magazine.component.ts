import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Magazine } from '../../models/magazine';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-magazine',
  templateUrl: './magazine.component.html',
  styleUrls: ['./magazine.component.css']
})
export class MagazineComponent implements OnInit {
  magazine: Magazine;
  constructor(public activatedRoute: ActivatedRoute, public sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.magazine = this.activatedRoute.snapshot.data['magazine'] || {};
  }
  get frame() {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.magazine.frame);
  }

}
