import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { MagazinesService } from '../../services/magazines.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MagazineResolver implements Resolve<any> {
    constructor( private magazinesService: MagazinesService) {}
    resolve(route: ActivatedRouteSnapshot): Observable<any> {
        return this.magazinesService.getMagazine(route.params['id']);
    }
}
