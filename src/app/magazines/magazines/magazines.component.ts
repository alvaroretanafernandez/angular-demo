import { Component, OnInit } from '@angular/core';
import { MagazinesService } from '../../services/magazines.service';
import { Magazine } from '../../models/magazine';

@Component({
  selector: 'app-magazines',
  templateUrl: './magazines.component.html',
  styleUrls: ['./magazines.component.css']
})
export class MagazinesComponent implements OnInit {
  magazines: Magazine[];
  constructor(public magazinesService: MagazinesService) { }
  ngOnInit() {
    this.fetchMagazines();
  }
  fetchMagazines() {
  this.magazinesService.getMagazines()
    .subscribe((data: Magazine[]) => {
      this.magazines = data;
      console.log(this.magazines);
    });
  }
}
