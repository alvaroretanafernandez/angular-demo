import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MagazinesComponent } from './magazines/magazines.component';
import { MagazineComponent } from './magazine/magazine.component';
import { MagazineResolver } from './magazine/magazine.resolver';

const routes: Routes = [
  { path: '', children: [
    { path: '', component: MagazinesComponent },
    { path: ':id', component: MagazineComponent,  resolve: { magazine: MagazineResolver },  pathMatch: 'full'},
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MagazinesRoutingModule { }
