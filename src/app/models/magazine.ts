export class Magazine {
  constructor(public id: number,
              public url: string,
              public frame: string,
              public img: string,
              public published: string) {
  }
}
