import { Injectable } from '@angular/core';
import { Magazine } from '../models/magazine';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class MagazinesService {
  public API = environment.API;
  constructor(public http: HttpClient) { }
  getMagazines() {
    return this.http.get<Magazine[]>(this.API);
  }
  getMagazine(id: number | string) {
    return this.http.get<Magazine>(this.API + id);
  }
}

