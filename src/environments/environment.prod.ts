export const environment = {
  production: true,
  menu: [ {
    name: 'Magazines',
    link: './magazines'
  }],
  API: 'https://api.stokedonfixedbikes.com/api/magazines/' // E.g : production.my-api.com
};
